test('primeiro teste com jest', () => {
  let number = null;
  expect(number).toBeNull();// verifica se o number é null
  number = 10;
  expect(number).not.toBeNull();// se não é null
  expect(number).toBe(10);// se é igual á
  expect(number).toEqual(10);
  expect(number).toBeGreaterThan(9); // maior que
  expect(number).toBeLessThan(11);// menor que 11
});

test('Devo saber trabalhar com objet', () => {
  const obj = { name: 'jon', mail: 'jon@test.com' };
  expect(obj).toHaveProperty('name');// se a propriedade exite
  expect(obj).toHaveProperty('name', 'jon');// segundo paramentro verifica o value
  expect(obj.name).toBe('jon');

  const obj2 = { name: 'jon', mail: 'jon@test.com' };
  expect(obj).toEqual(obj2);// compara o obt com o obj 2
  expect(obj).toBe(obj);// compara o obt vom ele mesmo
});

const { RuleTester } = require('eslint');
const request = require('supertest');
const jwt = require('jwt-simple');
const { response } = require('../../src/app');
const app = require('../../src/app');

const MAIN_ROUTE = '/v1/accounts';
let user;
let user2

beforeAll(async () => {
  const response = await app.services.users.save({
    name: 'testeAccount10', mail: `${Date.now()}@tes4t.com`, password: '123456',
  });
  const response2 = await app.services.users.save({
    name: 'usuario 2', mail: `${Date.now()}@tes4t.com`, password: '123456',
  });
  user = { ...response };/// concertar aqui
  user.token = jwt.encode(user, 'Segredo!');
  user2 = { ...response2 };
});

test('deve inserir uma conta com sucesso', () => {
  return request(app).post(MAIN_ROUTE)
    .send({ name: '#conta001'})
    .set('authorization', `bearer ${user.token}`)
    .then((response) => {
      expect(response.status).toBe(201);
      expect(response.body.name).toBe('#conta001');
    });
});

/*  VALIDAÇÃO DOS DADOS  */
test('Não de inserir uma conta sem nome', () => {
  return request(app).post(MAIN_ROUTE)
    .send({})
    .set('authorization', `bearer ${user.token}`)
    .then((response) => {
      expect(response.status).toBe(400);
      expect(response.body.error).toBe('Nome é um atributo obrigatorio');
    });
});

test('não deve inserir uma conta de nome duplicado, para o mesmo usuario', () => {
  return app.db('accounts').insert({name: 'Nome Duplicado', user_id: user.id})
    .then(() => request(app).post(MAIN_ROUTE)
      .set('authorization', `bearer ${user.token}`)
      .send({name: 'Nome Duplicado'}))
    .then((res) => {
      expect(res.status).toBe(400);
      expect(res.body.error).toBe('Já existe uma conta com esse mesmo nome');
    })
});
test('Deve listar apenas as contas do usuario',async () => {
  await app.db('transactions').del();
  await app.db('transfers').del();
  await app.db('accounts').del();
  return app.db('accounts').insert([
    { name: 'Account1', user_id: user.id},
    { name: 'Account2', user_id: user2.id}
  ])
    .then(() => request(app).get(`${MAIN_ROUTE}`)
      .set('authorization', `bearer ${user.token}`))
    .then((response) => {
      expect(response.status).toBe(200);
      expect(response.body.length).toBe(1);
      expect(response.body[0].name).toBe('Account1');
    })
});

/* test('Deve listar todas as contas', () => {
  return app.db('accounts').insert({name: 'teste list', user_id: user.id})
    .then(() => request(app).get(MAIN_ROUTE)
      .set('authorization', `bearer ${user.token}`))
    .then((response) => {
      expect(response.status).toBe(200);
      expect(response.body.length).toBeGreaterThan(0);
    });
}); */

test('Deve retornar uma conta por ID', () => {
  return app.db('accounts')
    .insert({ name: '#cont TEST', user_id: user.id })
    .then((account_id) => request(app).get(`${MAIN_ROUTE}/${account_id}`)
      .set('authorization', `bearer ${user.token}`))
    .then((response) => {
      // console.log(response.body);
      expect(response.status).toBe(200);
      expect(response.body.name).toBe('#cont TEST');
      expect(response.body.user_id).toBe(user.id);
    })
});

test('Deve alterar uma conta', () => { 
  return app.db('accounts').insert({name: '#conta update01', user_id: user.id})
    .then((account_id) => request(app).put(`${MAIN_ROUTE}/${account_id}`)
      .send({name: '#conta update02'})
      .set('authorization', `bearer ${user.token}`))
    .then((response) => {
      expect(response.status).toBe(200);
      expect(response.body.name).toBe('#conta update02');
    });
});

test('Não deve retornar uma conta de outro usuario', () => {
  return app.db('accounts')
    .insert({name: '#Account2', user_id: user2.id})
    .then((account_id) => request(app).get(`${MAIN_ROUTE}/${account_id}`)
      .set('authorization', `bearer ${user.token}`))
    .then((response) => {
      expect(response.status).toBe(403);
      expect(response.body.error).toBe('Este recurso não está disponivel para esse usuario');
    });
});


test('Não deve alterar uma conta de outro usuario', () => {
  return app.db('accounts')
    .insert({name: '#Account2', user_id: user2.id})
    .then((account_id) => request(app).put(`${MAIN_ROUTE}/${account_id}`)
      .send({name: 'update'})
      .set('authorization', `bearer ${user.token}`))
    .then((response) => {
      expect(response.status).toBe(403);
      expect(response.body.error).toBe('Este recurso não está disponivel para esse usuario');
    });
});

test('Deve remover uma conta', () => {
  return app.db('accounts')
    .insert({name: '#conta REMOVER', user_id: user.id,})
    .then((account_id) => request(app).delete(`${MAIN_ROUTE}/${account_id}`)
      .set('authorization', `bearer ${user.token}`))
    .then((response) => {
      expect(response.status).toBe(204);
      
    });
});


test('Não deve remover uma conta de outro usuario', () => {
  return app.db('accounts')
    .insert({name: '#Account2', user_id: user2.id})
    .then((account_id) => request(app).delete(`${MAIN_ROUTE}/${account_id}`)
      .set('authorization', `bearer ${user.token}`))
    .then((response) => {
      expect(response.status).toBe(403);
      expect(response.body.error).toBe('Este recurso não está disponivel para esse usuario');
    });
});
const request = require('supertest');
const app = require('../../src/app');


test('Deve criar usuario com signin', () => {
  request(app).post('/auth/signup')
    .send({name: 'Aldair', mail: `${Date.now()}@test.com`, password:'1234567'})
    .then((res) => {
      expect(res.status).toBe(201);
      expect(res.body.name).toBe('Aldair');
      expect(res.body).toHaveProperty('mail');
      expect(res.body).not.toHaveProperty('password');
    });
});

test('Deve receber o token ao logar', () => {
  const mail = `${Date.now()}@test.com`;
  return app.services.users.save(
    { name: 'teste auth', mail, password: '123456' }
  )
    .then(() => request(app).post('/auth/signin')
      .send({ mail, password: '123456' }))
    .then((res) => {
      expect(res.status).toBe(200);
      // console.log(res.body);
      expect(res.body).toHaveProperty('token');
    });
});
 
test('Não deve autenticar usuario com senha errada', () => {
  const mail = `${Date.now()}@test.com`;
  return app.services.users.save(
    { name: 'teste auth', mail, password: '123456' }
  )
    .then(() => request(app).post('/auth/signin')
      .send({ mail, password: '14644' }))
    .then((res) => {
      expect(res.status).toBe(400);
      // console.log(res.body);
      expect(res.body.error).toBe('Usuario ou senha errado');
    });
});

test('Não deve autenticar usuario não cadastrado', () => {
 
  return request(app).post('/auth/signin')
      .send({ mail: 'nãoexist@com', password: '14644' })
    .then((res) => {
      expect(res.status).toBe(400);
      expect(res.body.error).toBe('Usuario ou senha errado');
    });
});

test('Não deve acessar uma rota protegica com token', () => {
  return request(app).get('/v1/users')
    .then((res) => {
      expect(res.status).toBe(401);
    })
});

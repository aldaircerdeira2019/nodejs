const { response } = require('express');
const request = require('supertest');
const app = require('../../src/app');


const MAIN_ROUTE = '/v1/transfer';
/* para gerar o token usa o site ExtractJwt.io, não equece de incluir o segredo */
const TOKEN = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjEwMDAwIiwibmFtZSI6IlVzZXIgIzEiLCJtYWlsIjoidXNlcjFAZ2FtaWwuY29tIn0.ZTexpc2BdP4m9q_HO2UgZ4uiCpgqu3GnZff00uyMjqc';

beforeAll(async () => {
  /* await app.db.migrate.rollback();
  await app.db.migrate.latest(); */
  await app.db.seed.run();
},
/* timeout=100000 */
)
test('Deve listar apenas as transferencias do usuario', () => {
  return request(app).get(MAIN_ROUTE)
    .set('authorization', `bearer ${TOKEN}`)
    .then((response) => {
      expect(response.status).toBe(200);
      expect(response.body).toHaveLength(1);
      expect(response.body[0].description).toBe('Transfer #1');
    });
});

test('Deve inserir uma tranferencia com sucesso', () => {
  return request(app).post(MAIN_ROUTE)
    .set('authorization', `bearer ${TOKEN}`)
    .send({description: 'Regular Transfer', user_id:10000, account_origin_id:10000, account_dest_id:10001, ammount:100, date: '2021-01-01'})
    .then(async (response) => {
      expect(response.status).toBe(201);
      expect(response.body.description).toBe('Regular Transfer');

      const transactions = await app.db('transactions').where({ transfer_id: response.body.id})
      expect(transactions).toHaveLength(2);
      expect(transactions[0].description).toBe('Transfer to Acc #10001');
      expect(transactions[1].description).toBe('Transfer from Acc #10000');
      expect(transactions[0].ammount).toBe(-100);
      expect(transactions[1].ammount).toBe(100);
      expect(transactions[0].account_id).toBe(10000);
      expect(transactions[1].account_id).toBe(10001);
  });
});

describe('Ao salvar uma tranferencia valida ...', () => {
  let transfer_id;
  let income;
  let outcome;
  test('Deve retornar o status 201 e os dados da transferencia', () => {
    return request(app).post(MAIN_ROUTE)
      .set('authorization', `bearer ${TOKEN}`)
      .send({description: 'Regular Tranfer', user_id:10000, account_origin_id:10000, account_dest_id:10001, ammount:100, date: '2021-01-01'})
        .then((response) => {
          expect(response.status).toBe(201);
          expect(response.body.description).toBe('Regular Tranfer');
          transfer_id = response.body.id;
      });
  });
  
  test('As transações equivalentes devem ter sido geradas', async() => {
    const transactions = await app.db('transactions').where({ transfer_id: transfer_id}).orderBy('ammount');
      expect(transactions).toHaveLength(2);
      [outcome, income] = transactions;
    });
    
  test('As transações de saida deve ser negativas', () => {
    expect(outcome.description).toBe('Transfer to Acc #10001');
    expect(outcome.ammount).toBe(-100);
    expect(outcome.account_id).toBe(10000);
    expect(outcome.type).toBe('O');
  });

  test('As transações de entrada deve ser positivas', () => {
    expect(income.description).toBe('Transfer from Acc #10000');
    expect(income.ammount).toBe(100);
    expect(income.account_id).toBe(10001);
    expect(income.type).toBe('I');
  });

  test('Ambas devem referenciar a tranferencia que as originou', () => {
    expect(income.transfer_id).toBe(transfer_id);
    expect(outcome.transfer_id).toBe(transfer_id);
  });

  test('Ambas devem esta com o status realizados', () => {
    expect(income.status).toBe(1);
    expect(outcome.status).toBe(1);
  });

});

describe('Ao tentar salvar uma tranferencia invalida ...', () => {
  const validTranfer = {description: 'Regular Transfer', user_id:10000, account_origin_id:10000, account_dest_id:10001, ammount:100, date: '2021-01-01'};

  const template = (newDate, erroMessage) => {
    return request(app).post(MAIN_ROUTE)
      .set('authorization', `bearer ${TOKEN}`)
      .send({ ...validTranfer, ...newDate})
      .then((response) => {
        expect(response.status).toBe(400);
        expect(response.body.error).toBe(erroMessage);
      })
  }

  test('Não deve inserir sem descrição', () => template({description: null}, 'Descrição é um atributo obrigatorio'));

  test('Não deve inserir sem valor', () => template({ammount: null}, 'Valor é um atributo obrigatorio'));

  test('Não deve inserir sem data', () => template({date: null}, 'Data é um atributo obrigatorio'));

  test('Não deve inserir sem conta de origem', () => template({account_origin_id: null}, 'Conta de origem é um atributo obrigatorio'));

  test('Não deve inserir sem conta de destino', () => template({account_dest_id: null}, 'Conta de destino é um atributo obrigatorio'));

  test('Não deve inserir se as contas de origem e destino forem as mesmas', () => template({account_dest_id: 10000}, 'Não é possivel tranferir para a mesma conta'));

  test('Não deve inserir se as contas pertencerem a outro usuario', () => template({account_origin_id: 10002}, 'Conta não pertence ao usuario'));
});

test('Deve retorna uma tranferencia por Id', () => {
  return request(app).get(`${MAIN_ROUTE}/10000`)
    .set('authorization', `bearer ${TOKEN}`)
    .then((response) => {   
      expect(response.status).toBe(200);
      expect(response.body.description).toBe('Transfer #1');
    });
});


describe('Ao alterar uma tranferencia valida ...', () => {
  let transfer_id;
  let income;
  let outcome;
  test('Deve retornar o status 200 e os dados da transferencia', () => {
    return request(app).put(`${MAIN_ROUTE}/10000`)
      .set('authorization', `bearer ${TOKEN}`)
      .send({description: 'Transfer Updated Tranfer', user_id:10000, account_origin_id:10000, account_dest_id:10001, ammount:500, date: '2021-01-01'})
        .then((response) => {
          expect(response.status).toBe(200);
          expect(response.body.description).toBe('Transfer Updated Tranfer');
          expect(response.body.ammount).toBe(500);
          transfer_id = response.body.id;
      });
  });
  
  test('As transações equivalentes devem ter sido geradas', async() => {
    const transactions = await app.db('transactions').where({ transfer_id: transfer_id}).orderBy('ammount');
      expect(transactions).toHaveLength(2);
      [outcome, income] = transactions;
    });
    
  test('As transações de saida deve ser negativas', () => {
    expect(outcome.description).toBe('Transfer to Acc #10001');
    expect(outcome.ammount).toBe(-500);
    expect(outcome.account_id).toBe(10000);
    expect(outcome.type).toBe('O');
  });

  test('As transações de entrada deve ser positivas', () => {
    expect(income.description).toBe('Transfer from Acc #10000');
    expect(income.ammount).toBe(500);
    expect(income.account_id).toBe(10001);
    expect(income.type).toBe('I');
  });

  test('Ambas devem referenciar a tranferencia que as originou', () => {
    expect(income.transfer_id).toBe(transfer_id);
    expect(outcome.transfer_id).toBe(transfer_id);
  });

});


describe('Ao tentar alterar uma tranferencia invalida ...', () => {
  const validTranfer = {description: 'Regular Transfer', user_id:10000, account_origin_id:10000, account_dest_id:10001, ammount:100, date: '2021-01-01'};

  const template = (newDate, erroMessage) => {
    return request(app).put(`${MAIN_ROUTE}/10000`)
      .set('authorization', `bearer ${TOKEN}`)
      .send({ ...validTranfer, ...newDate})
      .then((response) => {
        expect(response.status).toBe(400);
        expect(response.body.error).toBe(erroMessage);
      })
  }

  test('Não deve inserir sem descrição', () => template({description: null}, 'Descrição é um atributo obrigatorio'));

  test('Não deve inserir sem valor', () => template({ammount: null}, 'Valor é um atributo obrigatorio'));

  test('Não deve inserir sem data', () => template({date: null}, 'Data é um atributo obrigatorio'));

  test('Não deve inserir sem conta de origem', () => template({account_origin_id: null}, 'Conta de origem é um atributo obrigatorio'));

  test('Não deve inserir sem conta de destino', () => template({account_dest_id: null}, 'Conta de destino é um atributo obrigatorio'));

  test('Não deve inserir se as contas de origem e destino forem as mesmas', () => template({account_dest_id: 10000}, 'Não é possivel tranferir para a mesma conta'));

  test('Não deve inserir se as contas pertencerem a outro usuario', () => template({account_origin_id: 10002}, 'Conta não pertence ao usuario'));
});

describe('Ao remover uma transferencia', () => {
  test('Deve retornar o status 204', () => {
    return request(app).delete(`${MAIN_ROUTE}/10000`)
      .set('authorization', `bearer ${TOKEN}`)
      .then((response) => {
        expect(response.status).toBe(204);
    });
  });

  test('O registro deve ter sido removido', () => {
    return app.db('transfers').where({id:10000})
    .then((result) => {
      expect(result).toHaveLength(0);
    });
  });

  test('As transações assossiadas devem ter sido removidas', () => {
    return app.db('transactions').where({transfer_id:10000})
    .then((result) => {
      expect(result).toHaveLength(0);
    });
  });
});


test('Não deve retornar transferencia de outro usuario', () => {
  return request(app).get(`${MAIN_ROUTE}/10001`)
    .set('authorization', `bearer ${TOKEN}`)
    .then((response) => {
      expect(response.status).toBe(403);
      expect(response.body.error).toBe('Este recurso não está disponivel para esse usuario');
      
  });
  
});
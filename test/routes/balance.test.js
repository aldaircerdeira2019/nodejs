const request = require('supertest');
const moment = require('moment');
const app = require('../../src/app');



const MAIN_ROUTE = '/v1/balance';
const ROUTE_TRANSACTION = '/v1/transactions';
const ROUTE_TRANSFER= '/v1/transfer';
/* para gerar o token usa o site ExtractJwt.io, não equece de incluir o segredo */
const TOKEN = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6MTAxMDAsIm5hbWUiOiJVc2VyICMzIiwibWFpbCI6InVzZXIzQGdhbWlsLmNvbSJ9.Wu3lmialFBcNdoLjlxmDr7mhSmmTFICa31al5774SmU';
const TOKEN_Geral = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6MTAxMDIsIm5hbWUiOiJVc2VyICM1IiwibWFpbCI6InVzZXI1QGdhbWlsLmNvbSJ9.5PiN7N0QCSQ2yY4UGlMvYot6zw82I3woAnheXwTxOQM';

beforeAll(async () => {
 /*  await app.db.migrate.rollback();
  await app.db.migrate.latest(); */
  await app.db.seed.run();
}/* ,timeout=100000 */);

describe('Ao calcular o saldo do usuario', () => {
  test('Deve retornar apenas as contas com alguma transação', () => {
    return request(app).get(MAIN_ROUTE)
      .set('authorization', `bearer ${TOKEN}`)
      .then((response) => {
        expect(response.status).toBe(200);
        expect(response.body).toHaveLength(0);
      });
  });
 
  test('Deve adicionar valores de entrada', () => {
    return request(app).post(ROUTE_TRANSACTION)
      .send({description:'1', date:  moment().format('YYYY-MM-D'), ammount: 100, type: 'I', account_id: 10100, status: true})
      .set('authorization', `bearer ${TOKEN}`)
      .then(() => {
        return request(app).get(MAIN_ROUTE)
          .set('authorization', `bearer ${TOKEN}`)
          .then((response) => {
            expect(response.status).toBe(200);
            expect(response.body).toHaveLength(1);
            expect(response.body[0].id).toBe(10100);
            expect(response.body[0].sum).toBe(100);
          });
      });
   });

  test('Deve subtrair valores de saida', () => { 
    return request(app).post(ROUTE_TRANSACTION)
      .send({description:'1', date:  moment().format('YYYY-MM-D'), ammount: 200, type: 'O', account_id: 10100, status: true})
      .set('authorization', `bearer ${TOKEN}`)
      .then(() => {
        return request(app).get(MAIN_ROUTE)
          .set('authorization', `bearer ${TOKEN}`)
          .then((response) => {
            expect(response.status).toBe(200);
            expect(response.body).toHaveLength(1);
            expect(response.body[0].id).toBe(10100);
            expect(response.body[0].sum).toBe(-100);
          });
      });
  });

  test('Não deve considerar transações pendentes', () => { 
    return request(app).post(ROUTE_TRANSACTION)
      .send({description:'1', date: moment().format('YYYY-MM-D'), ammount: 200, type: 'O', account_id: 10100, status: false})
      .set('authorization', `bearer ${TOKEN}`)
      .then(() => {
        return request(app).get(MAIN_ROUTE)
          .set('authorization', `bearer ${TOKEN}`)
          .then((response) => {
            expect(response.status).toBe(200);
            expect(response.body).toHaveLength(1);
            expect(response.body[0].id).toBe(10100);
            expect(response.body[0].sum).toBe(-100);
          });
      });
  });

  test('Não deve considerar saldo de contas distintas', () => { 
    return request(app).post(ROUTE_TRANSACTION)
      .send({description:'1', date: moment().format('YYYY-MM-D'), ammount: 50, type: 'I', account_id: 10101, status: true})
      .set('authorization', `bearer ${TOKEN}`)
      .then(() => {
        return request(app).get(MAIN_ROUTE)
          .set('authorization', `bearer ${TOKEN}`)
          .then((response) => {
            expect(response.status).toBe(200);
            expect(response.body).toHaveLength(2);
            expect(response.body[0].id).toBe(10100);
            expect(response.body[0].sum).toBe(-100);
            expect(response.body[1].id).toBe(10101);
            expect(response.body[1].sum).toBe(50);
          });
      });
  });
 
  test('Não deve considerar contas de outros usuarios', () => { 
    return request(app).post(ROUTE_TRANSACTION)
    .send({description:'1', date: moment().format('YYYY-MM-D'), ammount: 200, type: 'O', account_id: 10102, status: true})
    .set('authorization', `bearer ${TOKEN}`)
    .then(() => {
      return request(app).get(MAIN_ROUTE)
        .set('authorization', `bearer ${TOKEN}`)
        .then((response) => {
          expect(response.status).toBe(200);
          expect(response.body).toHaveLength(2);
          expect(response.body[0].id).toBe(10100);
          expect(response.body[0].sum).toBe(-100);
          expect(response.body[1].id).toBe(10101);
          expect(response.body[1].sum).toBe(50);
        });
    });
  });

  test('Deve considerar transações passadas', () => {
    return request(app).post(ROUTE_TRANSACTION)
    .send({description:'1', date: moment().subtract({days:5}).format('YYYY-MM-D'), ammount: 250, type: 'I', account_id: 10100, status: true})
    .set('authorization', `bearer ${TOKEN}`)
    .then(() => {
      return request(app).get(MAIN_ROUTE)
        .set('authorization', `bearer ${TOKEN}`)
        .then((response) => {
          expect(response.status).toBe(200);
          expect(response.body).toHaveLength(2);
          expect(response.body[0].id).toBe(10100);
          expect(response.body[0].sum).toBe(150);
          expect(response.body[1].id).toBe(10101);
          expect(response.body[1].sum).toBe(50);
        });
    });
  });

  test('Não deve considerar transações futuras', () => { 
    return request(app).post(ROUTE_TRANSACTION)
    .send({description:'1', date: moment().add({days:5}).format('YYYY-MM-D'), ammount: 250, type: 'O', account_id: 10100, status: true})
    .set('authorization', `bearer ${TOKEN}`)
    .then(() => {
      return request(app).get(MAIN_ROUTE)
        .set('authorization', `bearer ${TOKEN}`)
        .then((response) => {
          expect(response.status).toBe(200);
          expect(response.body).toHaveLength(2);
          expect(response.body[0].id).toBe(10100);
          expect(response.body[0].sum).toBe(150);
          expect(response.body[1].id).toBe(10101);
          expect(response.body[1].sum).toBe(50);
        });
    });
  });

  test('Deve considera transferencias', () => {
    return request(app).post(ROUTE_TRANSFER)
    .send({description:'1', date: moment().format('YYYY-MM-D'), ammount: 250, account_origin_id: 10100, account_dest_id: 10101})
    .set('authorization', `bearer ${TOKEN}`)
    .then((r) => {
      return request(app).get(MAIN_ROUTE)
        .set('authorization', `bearer ${TOKEN}`)
        .then((response) => {
          expect(response.status).toBe(200);
          expect(response.body).toHaveLength(2);
          expect(response.body[0].id).toBe(10100);
          expect(response.body[0].sum).toBe(-100);
          expect(response.body[1].id).toBe(10101);
          expect(response.body[1].sum).toBe(300);
        });
    });
  });
});

test('Deve calcular saldo das contas do usuario', () => {
  return request(app).get(MAIN_ROUTE)
  .set('authorization', `bearer ${TOKEN_Geral}`)
  .then((response) => {
    expect(response.status).toBe(200);
    expect(response.body).toHaveLength(2);
    expect(response.body[0].id).toBe(10104);
    expect(response.body[0].sum).toBe(162);
    expect(response.body[1].id).toBe(10105);
    expect(response.body[1].sum).toBe(-248);
  });
});


const request = require('supertest');
const jwt = require('jwt-simple');
const app = require('../../src/app');
const { response } = require('express');

const MAIN_ROUTE = '/v1/transactions';

let user = { name: '#user 01', mail: 'user01@test.com', password: '$2a$10$vrrcEeefCHabS80S0B.Ce.Eg3a9/td/GLV1B5pWzZk7mLTFlmnU3q' };
let user2 = { name: '#user 02', mail: 'user02@test.com', password: '$2a$10$vrrcEeefCHabS80S0B.Ce.Eg3a9/td/GLV1B5pWzZk7mLTFlmnU3q' };
let accUser;
let accUser2;
beforeAll(async () => {
  await app.db('transactions').del();
  await app.db('transfers').del();
  await app.db('accounts').del();
  await app.db('users').del();

  const user_id01 = await app.db('users').insert(user);
  const user_id02 = await app.db('users').insert(user2);

  [user.id] = user_id01;
  [user2.id] = user_id02;
  delete user.password;
  user.token = jwt.encode(user, 'Segredo!');

  accUser = { name: '#Acc 01', user_id: user.id };
  accUser2 = { name: '#Acc 02', user_id: user2.id };

  const acc_id = await app.db('accounts').insert(accUser);
  const acc_id2 = await app.db('accounts').insert(accUser2);

  [accUser.id] = acc_id;
  [accUser2.id] = acc_id2;



});
test('Deve listar apenas as transações do usuario', () => {
  return app.db('transactions').insert([
    { description: 'T1', date: new Date, ammount: 100, type: 'I', account_id: accUser.id },
    { description: 'T2', date: new Date, ammount: 100, type: 'O', account_id: accUser2.id }
  ]).then(() => request(app).get(MAIN_ROUTE)
    .set('authorization', `bearer ${user.token}`)
    .then((res) => {
      expect(res.status).toBe(200);
      expect(res.body).toHaveLength(1);
      expect(res.body[0].description).toBe('T1');
    }));
});

test('Deve inserir uma transação com sucesso', () => {
  return request(app).post(MAIN_ROUTE)
    .set('authorization', `bearer ${user.token}`)
    .send({description: 'New T', date: '2020-03-01', ammount: 100, type: 'I', account_id: accUser.id})
    .then((response) => {
      expect(response.status).toBe(201);
      expect(response.body.account_id).toBe(accUser.id);
  });
});

test('Transações de entrada devem ser positivas', () => {
  return request(app).post(MAIN_ROUTE)
    .set('authorization', `bearer ${user.token}`)
    .send({description: 'New T', date: '2020-03-01', ammount: -500, type: 'I', account_id: accUser.id})
    .then((transaction) =>  request(app).get(`${MAIN_ROUTE}/${transaction.body.id}`)
    .set('authorization', `bearer ${user.token}`)
    .then((response)=> {
      expect(transaction.status).toBe(201);
      expect(response.status).toBe(200);
      expect(response.body.ammount).toBe(500);
    }));
});

test('Transações de saida devem ser negativas', () => {
  return request(app).post(MAIN_ROUTE)
    .set('authorization', `bearer ${user.token}`)
    .send({description: 'New T', date: '2020-03-01', ammount: 500, type: 'O', account_id: accUser.id})
    .then((transaction) =>  request(app).get(`${MAIN_ROUTE}/${transaction.body.id}`)
    .set('authorization', `bearer ${user.token}`)
    .then((response)=> {
      expect(transaction.status).toBe(201);
      expect(response.status).toBe(200);
      expect(response.body.ammount).toBe(-500);
    }));
});

/* validação */
describe('Ao tentar inserir uma tranação invalida', () => {
  let validTransaction;
  beforeAll(() => {
    validTransaction = {description: 'New T', date: '2020-03-01', ammount: 500, type: 'O', account_id: accUser.id};
  })
  
  const testTemplate = (newData, ErrorMessage) => {
    return request(app).post(MAIN_ROUTE)
    .set('authorization', `bearer ${user.token}`)
    .send({...validTransaction, ...newData})
    .then((response) => {
        expect(response.status).toBe(400);
        expect(response.body.error).toBe(ErrorMessage);
      });
  };
  test('Não deve inserir uma transação sem descrição', () => testTemplate({description: null},'Descrição é um atributo obrigatorio'));
  test('Não deve inserir uma transação sem valor', () => testTemplate({ammount: null},'Valor é um atributo obrigatorio'));
  test('Não deve inserir uma transação sem data', () => testTemplate({date: null},'Data é um atributo obrigatorio'));
  test('Não deve inserir uma transação sem conta', () => testTemplate({account_id: null},'Id Conta é um atributo obrigatorio'));
  test('Não deve inserir uma transação sem tipo', () => testTemplate({type: null},'Tipo é um atributo obrigatorio'));
  test('Não deve inserir uma transação um tipo invalido', () => testTemplate({type: 'A'},'Tipo invalido'));
});

test('deve retorna uma transação por ID', () => {
  return app.db('transactions').insert(
    {description: 'T ID', date: '2020-03-01', ammount: 100, type: 'I', account_id: accUser.id}
  ).then(res => request(app).get(`${MAIN_ROUTE}/${res[0]}`)
      .set('authorization', `bearer ${user.token}`)
    .then((response) => {
      expect(response.status).toBe(200);
      expect(response.body.id).toBe(res[0]);
      expect(response.body.description).toBe('T ID');
    }));
});

test('Deve alterar uma transação', () => {
  return app.db('transactions').insert(
    {description: 'T Update', date: '2020-03-01', ammount: 100, type: 'I', account_id: accUser.id}
  ).then( res => request(app).put(`${MAIN_ROUTE}/${res[0]}`)
    .set('authorization', `bearer ${user.token}`)
    .send({ammount: 500})
    .then((response) => {
      expect(response.status).toBe(201);
      expect(response.body.id).toBe(res[0]);
      expect(response.body.ammount).toBe(500);
    }));
});

test('Deve remover uma tansação', () => {
  return app.db('transactions').insert(
    { description: 'T Delete', date: new Date, ammount: 100, type: 'O', account_id: accUser.id }
  ).then(trans => request(app).delete(`${MAIN_ROUTE}/${trans[0]}`)
    .set('authorization', `bearer ${user.token}`)
    .then((response) => {
      expect(response.status).toBe(204);
  }));
});

test('Não deve remover uma tansação de outro usuario', () => {
  return app.db('transactions').insert(
    { description: 'T Delete', date: new Date, ammount: 100, type: 'O', account_id: accUser2.id }
  ).then(trans => request(app).delete(`${MAIN_ROUTE}/${trans[0]}`)
    .set('authorization', `bearer ${user.token}`)
    .then((response) => {
      expect(response.status).toBe(403);
      expect(response.body.error).toBe('Este recurso não está disponivel para esse usuario');
  }));
});

test('Não deve remover conta com transação', () => {
  return app.db('transactions').insert(
    { description: 'T not Delete', date: new Date, ammount: 100, type: 'O', account_id: accUser.id }
  ).then(trans => request(app).delete(`/v1/accounts/${accUser.id}`)
    .set('authorization', `bearer ${user.token}`)
    .then((response) => {
      expect(response.status).toBe(400);
      expect(response.body.error).toBe('Essa conta possue transações assossiadas');
  }));
});
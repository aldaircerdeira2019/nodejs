const request = require('supertest');
const jwt = require('jwt-simple');

const app = require('../../src/app');

const mail = `${Date.now()}@test.com`;

const MAIN_ROUTE = '/v1/users';

let user;

beforeAll(async () => {
  const response = await app.services.users.save({
    name: 'testeAccount10', mail: `${Date.now()}@tes4t.com`, password: '123456',
  });
  user = { ...response };/// concertar aqui
  user.token = jwt.encode(user, 'Segredo!');
});


test('Deve listar todos os usuarios', () => {
  return request(app).get(MAIN_ROUTE)
    .set('authorization', `bearer ${user.token}`)
    .then((res) => {
      expect(res.status).toBe(200);
      // expect(res.body).toHaveLength(1); // verifica se a lista tem apenas 1 resultado , array
      expect(res.body.length).toBeGreaterThan(0);
      //expect(res.body[0]).toHaveProperty('name', 'Water Mitty');
    });
});

test('Deve inseri usuario com sucesso', () => {
  return request(app).post(MAIN_ROUTE).send({
    name: 'Water Mitty', mail, password: '124567',
  })
    .set('authorization', `bearer ${user.token}`)
    .then((res) => {
      expect(res.status).toBe(201);
      expect(res.body.name).toBe('Water Mitty');
      expect(res.body).not.toHaveProperty('password');//verifica a propriedade
    });
});

test('Deve armazenar senha criptografada', async () => {
  const res = await request(app).post(MAIN_ROUTE)
    .send({name: 'Water Mitty', mail:`${Date.now()}@test.com`, password: '124567',})
    .set('authorization', `bearer ${user.token}`);
  expect(res.status).toBe(201);
  const { id } = res.body;
  const userDB = await app.services.users.show({id});
  expect(userDB.password).not.toBe('124567');

});

test('Não deve inseri usuario sem nome', () => {
  return request(app).post(MAIN_ROUTE).send({mail: 'teste@teste', password: '123456'})
    .set('authorization', `bearer ${user.token}`)
    .then((res) => {
      expect(res.status).toBe(400);
      expect(res.body.error).toBe('nome invalido');
    });
});

test('Não deve inseri usuario sem email', () => {
  return request(app).post(MAIN_ROUTE).send({name: 'teste', password: '123456'})
    .set('authorization', `bearer ${user.token}`)
    .then((res) => {
      expect(res.status).toBe(400);
      expect(res.body.error).toBe('email invalido');
    });
});

test('Não deve inseri usuario sem password', () => {
  return request(app).post(MAIN_ROUTE).send({name: 'teste', mail: 'teste14555555@teste'})
    .set('authorization', `bearer ${user.token}`)
    .then((res) => {
      expect(res.status).toBe(400);
      expect(res.body.error).toBe('senha invalido');
    });
});

test('Não deve iserir usuario com email existente', () => {
  return request(app).post(MAIN_ROUTE).send({name: 'Water Mitty', mail, password: '124567'})
    .set('authorization', `bearer ${user.token}`)
    .then((res) => {
      expect(res.status).toBe(400);
      expect(res.body.error).toBe('Já exite usuario com esse email');
    })
});

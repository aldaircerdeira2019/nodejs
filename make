Usage: knex [options] [command]

Options:
  -V, --version                  output the version number
  --debug                        Run with debugging.
  --knexfile [path]              Specify the knexfile path.
  --cwd [path]                   Specify the working directory.
  --env [name]                   environment, default: process.env.NODE_ENV || development
  -h, --help                     output usage information

Commands:
  init [options]                         Create a fresh knexfile.
  migrate:make [options] <name>          Create a named migration file.
  migrate:latest                         Run all migrations that have not yet been run.
  migrate:rollback                       Rollback the last set of migrations performed.
  migrate:currentVersion                 View the current version for the migration.
  seed:make [options] <name>             Create a named seed file.
  seed:run                               Run seed files.

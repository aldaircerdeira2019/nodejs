module.exports = {
    test: {
      client: 'mysql',
      connection: {
        host:'172.17.0.1',
        port:'3309',
        user:'root',
        password: 'root',
        database: 'nodeapi'
      },
      migrations: {
        directory: 'src/migrations'
      },
      seeds: { directory: 'src/seeds'}
    }
};
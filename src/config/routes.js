/* NÃO TA SENDO USADO ! */

module.exports = (app) => {
  app.route('/auth/signin').post(app.routes.auth.signin);
  app.route('/auth/signup').post(app.routes.users.create);

  app.route('/users')
    .all(app.config.passport.authenticate())
    .get(app.routes.users.findAll)
    .post(app.routes.users.create);

  app.route('/accounts')
    .all(app.config.passport.authenticate())
    .post(app.routes.account.create)
    .get(app.routes.account.findAll);

  app.route('/accounts/:id')
    .all(app.config.passport.authenticate())
    .get(app.routes.account.show)
    .put(app.routes.account.update)
    .delete(app.routes.account.destroy);
};

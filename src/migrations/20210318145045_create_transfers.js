
exports.up = (knex, Promise) => {
    return Promise.all([
        knex.schema.createTable('transfers', (table) => {
            table.increments('id').primary();
            table.string('description').notNull();
            table.date('date').notNull();
            table.decimal('ammount', 15, 2).notNull();
            table.integer('account_origin_id').unsigned().notNull();
            table.integer('account_dest_id').unsigned().notNull();  
            table.integer('user_id').unsigned().notNull();

            table.foreign('account_origin_id').references('id').inTable('accounts');
            table.foreign('account_dest_id').references('id').inTable('accounts');
            table.foreign('user_id').references('id').inTable('users');

        }),
        knex.schema.table('transactions', (table) => {
            table.integer('transfer_id').unsigned();
            table.foreign('transfer_id').references('id').inTable('transfers');
        })
    ]);
  
};

exports.down = (knex, Promise) => {
    return Promise.all([
        knex.schema.table('transactions', (table) => {
            table.dropForeign('transfer_id');
        }),
        knex.schema.dropTable('transfers')
    ]);
};


exports.up = (knex) => {
    return knex.schema.createTable('transactions', (table) => {
        table.increments('id').primary();
        table.string('description').notNull();
        table.enu('type', ['I', 'O']).notNull();
        table.date('date').notNull();
        table.decimal('ammount',15,2).notNull();
        table.boolean('status').notNull().default(false);

        table.integer('account_id').unsigned().notNull();
        table.foreign('account_id').references('id').inTable('accounts');
    })
}

exports.down = (knex) => {
    return knex.schema.dropTable('transactions', (table) => {
        table.dropForeign('account_id');
    });
}
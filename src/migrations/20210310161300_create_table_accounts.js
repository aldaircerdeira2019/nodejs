exports.up = (knex) => knex.schema.createTable('accounts', (table) => {
  table.increments('id').primary();
  table.string('name').notNull();
  table.integer('user_id').unsigned().notNull();
  table.foreign('user_id').references('id').inTable('users');
});

exports.down = (knex) => knex.schema.dropTable('accounts', (table) => {
  table.dropForeign('user_id');
});

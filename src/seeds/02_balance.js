const moment = require('moment');
exports.seed = (knex) => {
  
  return knex('users').insert([
    {id: 10100, name: 'User #3', mail: 'user3@gamil.com', password: '$2a$10$vrrcEeefCHabS80S0B.Ce.Eg3a9/td/GLV1B5pWzZk7mLTFlmnU3q'},
    {id: 10101, name: 'User #4', mail: 'user4@gamil.com', password: '$2a$10$vrrcEeefCHabS80S0B.Ce.Eg3a9/td/GLV1B5pWzZk7mLTFlmnU3q'},
    {id: 10102, name: 'User #5', mail: 'user5@gamil.com', password: '$2a$10$vrrcEeefCHabS80S0B.Ce.Eg3a9/td/GLV1B5pWzZk7mLTFlmnU3q'}
  ])
    .then(() => knex('accounts').insert([
    {id:10100, name: 'Acc saldo Principal', user_id: 10100},
    {id:10101, name: 'Acc saldo Secundario', user_id: 10100},
    {id:10102, name: 'Acc Alternativa 1', user_id: 10101},
    {id:10103, name: 'Acc Alternativa 2', user_id: 10101},
    {id:10104, name: 'Acc geral Principal', user_id: 10102},
    {id:10105, name: 'Acc geral Principal', user_id: 10102},
  ]))
    .then(() => knex('transfers').insert([
    {id: 10100, description: 'Transfer #1', user_id: 10102, account_origin_id: 10105, account_dest_id: 10104, ammount: 256, date: new Date()},
    {id: 10101, description: 'Transfer #2', user_id: 10101, account_origin_id: 10102, account_dest_id: 10003, ammount: 512, date: new Date()},
  ])) 
   .then(() => knex('transactions').insert([
    //transação positiva saldo 2 
    { description: '2', date: new Date(), ammount: 2, type: 'I', account_id: 10104, status: true },
    //transação usuario errado saldo 4
     { description: '2', date: new Date(), ammount: 4, type: 'I', account_id: 10102, status: true },
    //transação para outra conta saldo 2/ outra conta 8
     { description: '2', date: new Date(), ammount: 8, type: 'I', account_id: 10105, status: true },
     //transação pendente conta saldo 2/ outra conta 8
    { description: '2', date: new Date(), ammount: 16, type: 'I', account_id: 10104, status: false },
   //transação passada conta saldo 34/ outra conta 8
    { description: '2', date:  moment().subtract({days:5}).format('YYYY-MM-D'), ammount: 32, type: 'I', account_id: 10104, status: true },
    //transação futura conta saldo 34/ outra conta 8
    { description: '2', date: moment().add({days:5}).format('YYYY-MM-D'), ammount: 64, type: 'I', account_id: 10104, status: true },
    //transação negativa conta saldo 94/ outra conta 8
     { description: '2', date: new Date(), ammount: -128, type: 'O', account_id: 10104, status: true },
    //transferencia  conta saldo 162/ outra conta -248
    { description: '2', date: new Date(), ammount: 256, type: 'I', account_id: 10104, status: true },
    { description: '2', date: new Date(), ammount: -256, type: 'O', account_id: 10105, status: true },
    //transferencia conta saldo 162/ outra conta -248
    { description: '2', date: new Date(), ammount: 512, type: 'I', account_id: 10103, status: true },
    { description: '2', date: new Date(), ammount: -512, type: 'O', account_id: 10102, status: true },
    /**/
  ])); 
    
};

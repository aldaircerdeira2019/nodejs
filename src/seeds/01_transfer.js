
exports.seed = function(knex, Promise) {
  // Deletes ALL existing entries
  return knex('transactions').del()
    .then(() => knex('transfers').del())
    .then(() => knex('accounts').del())
    .then(() => knex('users').del())

    .then(() => knex('users').insert([
      {id: 10000, name: 'User #1', mail: 'user1@gamil.com', password: '$2a$10$vrrcEeefCHabS80S0B.Ce.Eg3a9/td/GLV1B5pWzZk7mLTFlmnU3q'},
      {id: 10001, name: 'User #2', mail: 'user2@gamil.com', password: '$2a$10$vrrcEeefCHabS80S0B.Ce.Eg3a9/td/GLV1B5pWzZk7mLTFlmnU3q'}
    ]))
      .then(() => knex('accounts').insert([
        {id:10000, name: 'AccO #1', user_id: 10000},
        {id:10001, name: 'AccD #1', user_id: 10000},
        {id:10002, name: 'AccO #2', user_id: 10001},
        {id:10003, name: 'AccD #2', user_id: 10001},
      ]))
      .then(() => knex('transfers').insert([
        {id: 10000, description: 'Transfer #1', user_id: 10000, account_origin_id: 10000, account_dest_id: 10001, ammount: 100, date: new Date()},
        {id: 10001, description: 'Transfer #2', user_id: 10001, account_origin_id: 10002, account_dest_id: 10003, ammount: 100, date: new Date()},
      ]))
      .then(() => knex('transactions').insert([
        { description: 'Tranfer from AccO #1', date: new Date(), ammount: 100, type: 'I', account_id: 10001, transfer_id: 10000},
        { description: 'Tranfer to AccD #1', date: new Date(), ammount: -100, type: 'O', account_id: 10000, transfer_id: 10000},

        { description: 'Tranfer from AccO #2', date: new Date(), ammount: 100, type: 'I', account_id: 10003, transfer_id: 10001},
        { description: 'Tranfer to AccD #2', date: new Date(), ammount: -100, type: 'O', account_id: 10002, transfer_id: 10001},
      ]))
};

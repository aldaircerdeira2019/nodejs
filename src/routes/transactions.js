const { response, request } = require('express');
const RecursoIdevidoError = require('../errors/RecursoIdevidoError');
const express = require('express');
const { route } = require('../app');

module.exports = (app) => {
  const router = express.Router();

  router.param('id', (req, res, next) => {
    app.services.transactions.find(req.user.id, {'transactions.id': req.params.id})
      .then((result) => {
        if(result.length > 0)  next();
        else throw new RecursoIdevidoError();
      }).catch(err => next(err));
  });

  router.get('/', (req, res, next) => {
    app.services.transactions.find(req.user.id)
      .then((result) => res.status(200).json(result))
      .catch(err => next(err));
  });

  router.post('/', (req, res, next) => {
    app.services.transactions.save(req.body)
      .then(response => res.status(201).json(response))
      .catch((err) => next(err));
  });

  router.get('/:id', (req, res, next) => {
    app.services.transactions.findOne({id: req.params.id})
      .then((response) => res.status(200).json(response))
      .catch((err) => next(err));
  });

  router.put('/:id', (req, res, next) => {
    app.services.transactions.update(req.params.id, req.body)
      .then((response) => res.status(201).json(response))
      .catch((err) => next(err))
  });

  router.delete('/:id', (req, res, next) => {
    app.services.transactions.destroy(req.params.id)
      .then(() => res.status(204).json('ok'));
  });

  return router;
}
const express = require('express');
const RecursoIdevidoError = require('../errors/RecursoIdevidoError');

module.exports = (app) => {
  const router = express.Router();

  router.param('id', (req, res, next) => {
    app.services.account.show({id: req.params.id})
      .then((acc) => {
        if(acc.user_id != req.user.id) throw new RecursoIdevidoError();
        else next();
      }).catch(err => next(err));
  });

  router.get('/', (req, res, next) => {
    app.services.account.findAll(req.user.id).then((response) => {
      res.status(200).json(response);
    }).catch(err => next(err));
  });

  router.post('/', (req, res, next) => {
    //console.log(req.user);
    app.services.account.save({...req.body, user_id: req.user.id}).then((r) =>{
      res.status(201).json(r);
    }).catch(err => next(err));
  });

  router.get('/:id', (req, res) => 
    app.services.account.show({ id: req.params.id })
      .then((r) => {
        //if(r.user_id != req.user.id) return res.status(403).json({error:'Este recurso não está disponivel para esse usuario'});
         res.status(200).json(r);
      }).catch(err => next(err))
  );

  router.put('/:id', (req, res, next) => {
    app.services.account.update(req.params.id, req.body).then((r) => {
      res.status(200).json(r);
    }).catch(err => next(err));
  });

  router.delete('/:id',(req, res, next) => {
    app.services.account.destroy(req.params.id).then(() => {
      res.status(204).send();
    }).catch(err => next(err));
  });

  return router;
};

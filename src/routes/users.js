
const express = require('express');

module.exports = (app) => {
  const router = express.Router();

  router.get('/', (req, res, next) => {
    app.services.users.findAll().then((response) => {
      res.status(200).json(response);
    }).catch(err => next(err));
  });

  router.post('/', (req, res, next) => {
    app.services.users.save(req.body).then((r) => {
      res.status(201).json(r);
    }).catch(err => next(err));
  });

  return router ;
};

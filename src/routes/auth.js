const express = require('express');
const jwt = require('jwt-simple');
const bcrypt = require('bcrypt-nodejs');
const ValidationError = require('../errors/ValidationError');

const secret = 'Segredo!';

module.exports = (app) => {
  const router = express.Router();

  router.post('/signin', (req, res, next) => {
    app.services.users.show({ mail: req.body.mail })
      .then((user) => {
      if(!user) throw new ValidationError('Usuario ou senha errado');
      if (bcrypt.compareSync(req.body.password, user.password)) {
        const payload= {
          id: user.id,
          name: user.name,
          mail: user.password
        };
        const token = jwt.encode(payload, secret);
        res.status(200).json({token});
      } else throw new ValidationError('Usuario ou senha errado');
    }).catch(err => next(err));
  });


  router.post ('/signup',(req, res, next) => {
    app.services.users.save(req.body).then((r) => {
      res.status(201).json(r);
    }).catch(err => next(err));
  });


  return router;
}
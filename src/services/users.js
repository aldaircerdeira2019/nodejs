const bcrypt =require('bcrypt-nodejs');
const ValidationError = require('../errors/ValidationError');

module.exports = (app) => {
  const findAll = () => app.db('users').select('id', 'name', 'mail');

  const show = (filter = {}) => app.db('users').where(filter).first();

  const getPasswordHash = (password) => {
    const salt = bcrypt.genSaltSync(10);
    return bcrypt.hashSync(password, salt);
  }
  const save = async(user) => {
    const email = user.mail != null ? user.mail : null; 
    const userDB = await show({ mail: email});
    if (!user.name) throw new ValidationError ('nome invalido');
    if (!email) throw new ValidationError ('email invalido'); 
    if (!user.password) throw new ValidationError ('senha invalido');
    if (email && userDB) throw new ValidationError ('Já exite usuario com esse email'); 
    
    const newUser = { ...user}; // criar um novo objto apartir do objt que veio como paramentro
    user.password = getPasswordHash(user.password);

    return app.db('users').insert(user).then((r) => {
      user.id = r[0];
      delete user.password;
      return user;
    });
  };

  return {
    findAll,
    save,
    show
  };
};

const { response } = require("express");
const { route } = require("../app");
const ValidationError = require('../errors/ValidationError');

module.exports = (app) => {
  const find = (userId, filter = {}) => {
    return app.db('transactions')
      .join('accounts', 'accounts.id', 'account_id')
      .where(filter)
      .andWhere('accounts.user_id', '=', userId)
      .select();

  };

  const save = (transaction) => {
    /* validação */
    if(!transaction.description) throw new ValidationError ('Descrição é um atributo obrigatorio');
    if(!transaction.ammount) throw new ValidationError ('Valor é um atributo obrigatorio');
    if(!transaction.date) throw new ValidationError ('Data é um atributo obrigatorio');
    if(!transaction.account_id) throw new ValidationError ('Id Conta é um atributo obrigatorio');
    if(!transaction.type) throw new ValidationError ('Tipo é um atributo obrigatorio');
    if(!(transaction.type === 'I' || transaction.type === 'O')) throw new ValidationError ('Tipo invalido');

    const newTrasaction = { ...transaction}
    if((transaction.type === 'I' && transaction.ammount < 0) || (transaction.type === 'O' && transaction.ammount >0)){
      newTrasaction.ammount *= -1;
    }
    return app.db('transactions')
      .insert(newTrasaction)
      .then((response)  => {
        newTrasaction.id = response[0];
        return newTrasaction;
      });
  }

  const findOne = (filter) => {
    return app.db('transactions')
      .where(filter)
      .first();
  };

  const update = (id, transaction) => {
    return app.db('transactions')
    .where({id})
    .update(transaction)
    .then(() => {
      transaction.id = Number(id);
      return transaction;
    })
  };

  const destroy = (id) => {
    return app.db('transactions')
    .where({id})
    .delete();
  }

  return {find, save, findOne, update, destroy};
}
const ValidationError = require('../errors/ValidationError');

module.exports = (app) => {
  const find = (filter = {}) => {
    return app.db('transfers')
      .where(filter)
      .select();
  };
  const findOne = (filter = {}) => {
    return app.db('transfers')
      .where(filter)
      .first();
  };

  const validate = async (transfer) => {
    if(!transfer.description) throw new ValidationError ('Descrição é um atributo obrigatorio');
    if(!transfer.ammount) throw new ValidationError ('Valor é um atributo obrigatorio');
    if(!transfer.date) throw new ValidationError ('Data é um atributo obrigatorio');
    if(!transfer.account_origin_id) throw new ValidationError ('Conta de origem é um atributo obrigatorio');
    if(!transfer.account_dest_id) throw new ValidationError ('Conta de destino é um atributo obrigatorio');
    if(transfer.account_origin_id === transfer.account_dest_id ) throw new ValidationError ('Não é possivel tranferir para a mesma conta');

    const accounts = await app.db('accounts').whereIn('id', [transfer.account_dest_id, transfer.account_origin_id]).select();
    
    accounts.forEach((acc) => {
      if(acc.user_id !== parseInt(transfer.user_id, 10)) throw new ValidationError ('Conta não pertence ao usuario')
    });

  }

  const save = async (transfer) => {
   
    const transfer_id = await app.db('transfers').insert(transfer);
    transfer.id = transfer_id[0];
    const result = transfer;
   
    const transactions = [
      {description: `Transfer to Acc #${transfer.account_dest_id}`, date: transfer.date, ammount: transfer.ammount * -1, type:'O', account_id: transfer.account_origin_id, transfer_id: transfer.id, status:true},
      {description: `Transfer from Acc #${transfer.account_origin_id}`, date: transfer.date, ammount: transfer.ammount, type:'I', account_id: transfer.account_dest_id, transfer_id: transfer.id, status:true},
    ];
    
    await app.db('transactions').insert(transactions);

    return result;
 
  };

  const update = async (id, transfer) => {
   
    let newTranfer;
    const result = await app.db('transfers')
    .where({id})
    .update(transfer);
    if(result)  newTranfer = { ...transfer, id: Number(id)};
   
      

    const transactions = [
      {description: `Transfer to Acc #${newTranfer.account_dest_id}`, date: newTranfer.date, ammount: newTranfer.ammount * -1, type:'O', account_id: newTranfer.account_origin_id, transfer_id: id, status:true},
      {description: `Transfer from Acc #${newTranfer.account_origin_id}`, date: newTranfer.date, ammount: newTranfer.ammount, type:'I', account_id: newTranfer.account_dest_id, transfer_id: id,status:true},
    ];
    await app.db('transactions').where({transfer_id: id}).del();
    await app.db('transactions').insert(transactions);

    return newTranfer;
  };

  const destroy = async (id) => {
    await app.db('transactions').where({transfer_id: id}).del();
    return app.db('transfers').where({id}).del();
  }

  return { find, save, findOne, update, validate, destroy};
};
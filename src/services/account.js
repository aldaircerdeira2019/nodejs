const { response } = require('express');
const account = require('../routes/account');
const ValidationError = require('../errors/ValidationError');
const { use } = require('../app');

module.exports = (app) => {
  const findAll = (user_id) => app.db('accounts').where({user_id}).select();

  const save = async (account) => {
    
    if(!account.name)    throw new ValidationError ('Nome é um atributo obrigatorio');
    if(!account.user_id)   throw new ValidationError ( 'user_id é um atributo obrigatorio');
    
    const accDB = await show({name: account.name, user_id: account.user_id});
    if(accDB) throw new ValidationError ('Já existe uma conta com esse mesmo nome');

    return app.db('accounts').insert(account).then((r) => {
      account.id = r[0];
      return account;
    });
  };

  const show = (filter = {}) => app.db('accounts').where(filter).first();

  const update = (id, account) => app.db('accounts').where({ id }).update(account).then((r) => {
    account.id = Number(id);
    return account;
  });

  const destroy = async (id) => {
    const transaction = await app.services.transactions.findOne({account_id: id});
    if(transaction) throw new ValidationError ('Essa conta possue transações assossiadas');

    return app.db('accounts').where({id}).delete();
  }

  return {
    save, findAll, show, update, destroy
  };
};
